//All of the includes that we will be using
const path = require('path');
const { promisify } = require('util');
const fs = require('fs');
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//loads the json from a file
const readFile = promisify(fs.readFile);

//These 2 objects make the graph structure that we will be using
const actors = {};
const movies = {};

//These are links back to the original json becuase JSON.stringify
//Does not work on "Circular Structures" such as a graph. The final
//Object sent comes from here, but the graph travesal happens above
let actorStrings = {};
let movieStrings = {};

//Loads the json into the graph structures
const loadFromFile = async () => {
    //Read the json object from the file
    const JSON_FILE = path.join(__dirname, 'data.json');
    const struct = JSON.parse(await readFile(JSON_FILE));

    //Separate into movies and actors
    actorStrings = struct[0];
    movieStrings = struct[1];

    //For all of the actors
    for(actor in actorStrings) {
        //Get the current actor that we are on
        let currentActor = actorStrings[actor];

        //Create an actor Object to represent to this actor
        let newActor = {
            name:  currentActor.name ,
            age: currentActor.age,
            total_gross: currentActor.total_gross,
            movies: []
        }

        //For all of this actor's movies we create a movie object
        let newMovies = [];
        for(movie in currentActor.movies) {

            //Get the name of this movie
            currentMovieName = currentActor.movies[movie];

            //If the global movies array already contains this movie then we just push the new actor to the list
            if(movies[currentMovieName]){
                movies[currentMovieName].actors.push(newActor);
                newMovies.push(movies[currentMovieName]);
            }
            //Else we create a new movie
            else {
                newMovie = {
                    name: currentMovieName,
                    wiki_page: "",
                    box_office: "0",
                    year: "",
                    actors: [newActor]
                }
                newMovies.push(newMovie);
                movies[currentMovieName] = newMovie;
            }
        }

        //Now we push the actor into the graph
        newActor.movies = newMovies;
        actors[newActor.name] = newActor;
    }

    //Now we need to loop over all the movies and fill in the rest of their information
    for(movie in movieStrings) {
        //If the movie was not acted in by any actor, we simply ignore it
        if(movies[movie]) {
            currentMovie = movieStrings[movie];
            movies[movie].wiki_page = currentMovie.wiki_page;
            movies[movie].box_office = currentMovie.box_office;
            movies[movie].year = currentMovie.year;
        }
    }
    return struct;
}

//This returns a map which maps actors the the number of actors that they are connected to
const getHubMap = async () => {
    let hubMap = {};

    //For every actor
    for(actor in actors) {
        let relatedActors = []
        let currentActor = actors[actor];
        //For all of his movies
        for(movie in currentActor.movies) {
            //Grab all of their actors and combine them
            relatedActors = relatedActors.concat(movies[currentActor.movies[movie].name].actors);
        }
        let uniqueActors = new Set(relatedActors);
        hubMap[actor] = uniqueActors;
    }
    return hubMap;
}

//This uses "getHubMap" to find the "i" most connected actors and returns them as a list
const findHubs = async (i) => {
    let hubMap = await getHubMap();
    const rtn = [];
    for(let j = 0; j < i; j++) {
        let maxSize = 0;
        let foundObj = {};
        for(obj in hubMap) {
            if(hubMap[obj].size > maxSize) {
                foundObj = obj;
                maxSize = hubMap[obj].size;
            }
        }
        rtn.push(foundObj);
        delete hubMap[foundObj];
    }
    return rtn;
}

//This is a function that gets the money for ages in the range (i,j)
const getMoneyForAge = async (i, j) => {
    let sum = 0;
    for(actor in actors) {
        if(actors[actor].age > i && actors[actor].age < j) {
            sum += actors[actor].total_gross;
        }
    }
    return sum;
}

//This is a function that allows us to correlate the age of an actor with his connecteness

const getAgeForConnectedness = async () => {
    const hubMap = await getHubMap();
    let numTotalConnections = 0;
    let weightedAvg = 0;
    for(obj in hubMap) {
        numTotalConnections += hubMap[obj].size;
    }
    for(obj in hubMap) {
        let percentageConnections = hubMap[obj].size / numTotalConnections;
        weightedAvg += actors[obj].age * percentageConnections;
    }
    return weightedAvg;
 }

/*
 * Below are all the functions for the REST API
 */


app.get('/actors', function (req, res) {
    if(req.query["name"]) {
        console.log("Actor by name");
        res.send(actorStrings[req.query.name]);
    }
    else {
        res.send("OK");
    }
 })

 app.get('/movies', function (req, res) {
    console.log(req.query);
    if(req.query["name"]) {
        console.log("movie by name");
        res.send(movieStrings[req.query.name]);
    }
    else if(req.query["wiki_page"]) {
        console.log("Movie by wiki");
    }
    else {
        res.send("OK");
    }
 })

 app.delete('/actors', function (req, res) { 
    if(req.query["name"]) {
        console.log("Deleting actor by name");
        delete actorStrings[req.query.name];
        delete actors[req.query.name];
        res.send("OK");
    }
    else {
        res.send("OK");
    }
 })


 app.delete('/movies', function (req, res) {
    console.log(req.query);
    if(req.query["name"]) {
        console.log("delete movie by name");
        delete movieStrings[req.query.name];
        delete movie[req.query.name];
        res.send("OK");
    }
    else if(req.query["wiki_page"]) {
        console.log("Movie by wiki");
    }
    else {
        res.send("OK");
    }
 })

 app.post('/actors', function (req, res) {
    console.log(req.body);
    if(req.body["name"]) {
        let age = 0;
        let total_gross = 0;
        let movies = [];
        if(req.body["age"])
            age = req.body["age"];
        if(req.body["total_gross"])
            total_gross = req.body["total_gross"];
        if(req.body["movies"]);
            movies = req.body["movies"];
        let actor = {
            name:  req.body["name"] ,
            age,
            total_gross,
            movies
        }
        actors[req.body["name"]] = actor;
        actorStrings[req.body["name"]] = actor;
        res.send(201);
        res.send(actor);
    }
    else {
        res.status(400).end();
    }
 })

 app.post('/movies', function (req, res) {
    console.log(req.body);
    if(req.body["name"]) {
        let wiki_page = 0;
        let box_office = 0;
        let year = 0;
        let actors = [];
        if(req.body["wiki_page"])
            wiki_page = req.body["wikipage"];
        if(req.body["box_office"])
            box_office = req.body["box_office"];
        if(req.body["year"]);
            year = req.body["year"];
        if(req.body["actors"]);
            actors = req.body["actors"]; 
        let movie = {
            "name": req.body["name"],
            wiki_page,
            box_office,
            year,
            actors
        }
        movies[req.body["name"]] = movie;
        movieStrings[req.body["name"]] = movie;
        res.send(movie);
        res.end("201");
    }
    else {
        res.send("Invalid input");
    }
 })

 app.put("/actors", function (req, res) {
    console.log(req.body);
    console.log(req.query.name);
    if(req.body["age"]) {
        actors[req.query.name]['age'] = req.body["age"];
        actorStrings[req.query.name]['age'] = req.body["age"];
    }
    if(req.body["total_gross"]) {
        actors[req.query.name]['total_gross'] = req.body["total_gross"];
        actorStrings[req.query.name]['total_gross'] = req.body["total_gross"];
    }
    res.send(201);
 })


 app.put("/movies", function (req, res) {
    console.log(req.body);
    console.log(req.query.name);
    if(req.body["wiki_page"]) {
        actors[req.query.name]['wiki_page'] = req.body["wiki_page"];
        actorStrings[req.query.name]['wiki_page'] = req.body["wiki_page"];
    }
    if(req.body["box_office"]) {
        actors[req.query.name]['box_office'] = req.body["box_office"];
        actorStrings[req.query.name]['box_office'] = req.body["box_office"];
    }
    if(req.body["year"]) {
        actors[req.query.name]['year'] = req.body["year"];
        actorStrings[req.query.name]['year'] = req.body["year"];
    }
    res.send(actors[req.query.name]);
    res.send(201);
})


/*
 * Below this point is the "Main" function and the function that starts the server
 * and listens for restful requests
 */


//The main function that runs our test code.
;(async () => {
    let structure = await loadFromFile();
    let hubs = await findHubs(6);
    let ageBucketSize = 15;
    let numBuckets = 8;
    console.log(hubs);
    let ageGroups = [];
    for(let i = 0; i < numBuckets; i ++) {
        ageGroups.push(await getMoneyForAge(ageBucketSize * i, ageBucketSize * (i + 1)));
    }
    console.log(ageGroups);
    console.log(await getAgeForConnectedness());
    console.log("Hell yea");
})();

const server = app.listen(8081, function () {

    var host = server.address().address
    var port = server.address().port

    console.log("Example app listening at http://%s:%s", host, port)
  
  })
