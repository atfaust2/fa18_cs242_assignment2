//Includes
const fs = require('fs');
const path = require('path');
const { promisify } = require('util');
const fetch = require("node-fetch");
const cheerio = require("cheerio");
const winston = require('winston');

//This is our logger, if we want to change the log level we do it here
const logger = winston.createLogger({
    level: 'debug',
    transports: [
      new winston.transports.Console(),
      new winston.transports.File({ filename: 'combined.log' })
    ]
  });

//This is the JSON file that we will write to
const JSON_FILE = path.join(__dirname, 'graph.json');

//This forms the basis of our graph structure 
//Since the graph is bipartitie, 2 sets is a good way to represent it.
const allActors = {};
const allMovies = {};
let root;

//This helps us to write to files asynchronously
const writeFile = promisify(fs.writeFile);
const readFile = promisify(fs.readFile);

//Saves to a file the graph structure
const saveToFile = async () => {
    await writeFile(JSON_FILE, JSON.stringify(root));
}

//loads the json from a file
const loadFromFile = async () => {
    root = JSON.parse(await readFile(JSON_FILE));
}

// Looks up the actor and gets his associated movies, pushing them to his
// Movie array. Returns an actor object
const getActor = async wikiLink => {
    logger.log({
        level: 'debug',
        message: `Called getActor with args ${wikiLink}`
      });
    const res = await fetch(wikiLink);
    const data = await res.text();
    const $ = cheerio.load(data);
    const movies = [];
    $('#Filmography').parent().next().next().children().each((_, e) => {
        const a = $(e).find('i > a');
        if(a.attr('href')) {
            if(allMovies[a.attr('href')]) {
                movies.push(allMovies[a.attr('href')]);
            }
            else {
                movies.push({
                    name: a.text(),
                    href: a.attr('href'),
                    actors: []
                });
            }
        }
    });
    const ret = {
        name: 'Faust',
        href: wikiLink,
        movies
    };
    allActors[wikiLink] = ret;
    return ret;
}

// Looks up the movie and gets its associated actors, pushing them to its
// actor array. Returns a movie object
const getMovie = async wikiLink => {
    logger.log({
        level: 'debug',
        message: `Called getMovie with args ${wikiLink}`
      });
    const res = await fetch(wikiLink);
    const data = await res.text(); 
    const $ = cheerio.load(data); 
    const actors = [];
       $('#Cast').parent().next().children().each((_, e) => {
        const a = $(e).find('a');
        if(a.attr('href')) {
            if(allActors[a.attr('href')]) {
                movies.push(allActors[a.attr('href')]);
            }
            else {
                actors.push({
                    name: a.text(),
                    href: a.attr('href'),
                    movies: []
                });
            }
        }
    });
    const ret = {
        name: 'Faust: The Reckoninng',
        href: wikiLink,
        actors
    };
    allMovies[wikiLink] = ret;
    return ret;
}

//The main function that runs our test code.
;(async () => {
    root = await getActor("https://en.wikipedia.org/wiki/Seth_MacFarlane");
    await recursiveSearch(root, 2);
    for(let i of Object.keys(allActors)) {
        logger.log({
            level: 'silly',
            message: i
          });
        }
    for(let i of Object.keys(allMovies)) {
        logger.log({
            level: 'silly',
            message: i
          });
    }
    root = allMovies['https://en.wikipedia.org/wiki/Stewie_Griffin:_The_Untold_Story'];
    saveToFile();
})();

//A function that allows us to sleep
const sleep = ms => new Promise(r => setTimeout(r, ms));

//The top level of the recursive search, specifies a starting point
//and the depth at which we should search
const recursiveSearch = async (startingLink, depth) => {
    logger.log({
        level: 'debug',
        message: `Called getMovie with args ${startingLink} and ${depth}`
      });
    await recursiveSearchHelper([startingLink], true, depth);
}


//The helper that does all of the actual recursion to look up the actors
//And the movies using the getActor and getMovie functions.

//It knows which it is looking for basied on the actorSearch boolean 
const recursiveSearchHelper = async (searchList, actorSearch, depth) => {
    logger.log({
        level: 'debug',
        message: `Called getMovie with args ${searchList} and ${actorSearch} and ${depth}`
      });
   if(depth <= 0) {
       return searchList;
   }
   if(actorSearch) {
       await Promise.all(searchList.map(async actor => {
        logger.log({
            level: 'debug',
            message: `Actor searching for movies ${actor.movies}`
          });
           const movies = await Promise.all(actor.movies.map(({ href }) => getMovie(`https://en.wikipedia.org${href}`)));
           await recursiveSearchHelper(movies, !actorSearch ,depth - 1);
       }));
   }
   if(!actorSearch) {
        await Promise.all(searchList.map(async movie => {
           logger.log({
            level: 'debug',
            message: `Movie searching for actor ${movie.href}`
          });
           let fakeMovie = await getMovie(movie.href);
           allMovies[movie.href].actors = fakeMovie.actors;
           let a = allActors;
           /*const actors = await Promise.all(movie.actors.map(actor => {
                console.log(`Loooking up: ${actor.href}`);
                allMovies[item.href].actors.push(getActor(`https://en.wikipedia.org${actor.href}`));
           }));*/
           await recursiveSearchHelper(movie.actors, !actorSearch ,depth - 1);
       }));
   }
}





